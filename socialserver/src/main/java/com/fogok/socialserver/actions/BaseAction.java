package com.fogok.socialserver.actions;

import com.fogok.socialserver.config.SocSrvConfig;

/**
 * Base action for all server actions
 * @param <T> request
 * @param <R> response
 */
public abstract class BaseAction<T, R> {

    protected SocSrvConfig config;

    public BaseAction(SocSrvConfig config) {
        this.config = config;
    }

    public abstract R run(T request);

}
