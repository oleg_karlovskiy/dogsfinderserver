package com.fogok.socialserver.config;

import com.fogok.spaceshipserver.config.BaseConfigModel;

public class SocSrvConfig extends BaseConfigModel {

    @Override
    public void createDefaultConfigModel() {
        params.put("port", "15503");
    }

}
