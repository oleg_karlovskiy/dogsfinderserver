package com.fogok.socialserver;

import com.fogok.socialserver.config.SocSrvConfig;
import com.fogok.spaceshipserver.BaseTcpChannelInboundHandlerAdapter;
import com.google.protobuf.GeneratedMessageV3;

import io.netty.channel.ChannelHandlerContext;

import static com.esotericsoftware.minlog.Log.info;

public class SocSrvHandlerTcp extends BaseTcpChannelInboundHandlerAdapter<GeneratedMessageV3, SocSrvConfig> {


    @Override
    public void init() {

    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GeneratedMessageV3 msg) throws Exception {

    }


    @Override
    public void handlerAdded(ChannelHandlerContext ctx) {
        info(String.format("Client %s joined to %s service", ctx.channel().remoteAddress(), this.getClass().getSimpleName()));
    }


    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) {
        info(String.format("Client %s left in %s service", ctx.channel().remoteAddress(), this.getClass().getSimpleName()));

    }
}
