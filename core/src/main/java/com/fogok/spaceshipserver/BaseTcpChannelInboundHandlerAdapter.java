package com.fogok.spaceshipserver;

import com.fogok.spaceshipserver.config.BaseConfigModel;

import io.netty.channel.SimpleChannelInboundHandler;

public abstract class BaseTcpChannelInboundHandlerAdapter<R, T extends BaseConfigModel> extends SimpleChannelInboundHandler<R> {

    protected T config;

    public void init(T config){
        setConfig(config);
        init();
    }

    public abstract void init();

    public void setConfig(T config) {
        this.config = config;
    }

    public T getConfig() {
        return config;
    }
}
